<?php

namespace App\Filters;

use App\Filters\Filters;
use App\Models\Career;
use Shop\Models\Product;

class CustomerFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'id',
        'email',
        'rfc',
    ];

    /**
     * Filter the query by a given label.
     *
     * @param  string $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function other($search)
    {
        $productsIds = Product::where('label', 'like', "%{$search}%")->pluck('id');

        return $this->builder->whereIn('id', $productsIds->toArray());
    }

    protected function id($id)
    {
        return $this->builder->where('cve_cte', (int) $id);
    }

    protected function rfc($rfc)
    {
        return $this->builder->where('rfc_cte', $rfc);
    }

    protected function email($email)
    {
        return $this->builder->where('email_cte', $email);
    }

    protected function by_start_date($orderCriteria)
    {
        $this->builder->getQuery()->orders = [];

        switch ($orderCriteria) {
            case 'current_month':
                return $this->builder->whereYear('start', now()->year)->whereMonth('start', now()->month);
            case 'last_month':
                return $this->builder->whereYear('start', now()->subMonth()->year)->whereMonth('start', now()->subMonth()->month);
            case 'current_year':
                return $this->builder->whereYear('start', now()->subMonth()->year);
        }

        return $this->builder;
    }

    /**
     * Filter the query by a given category.
     *
     * @param  string $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function by_category($categoryId)
    {
        return $this->builder->where('product_category_id', $categoryId);
    }

    protected function specialty_id($specialtyId)
    {
        return $this->builder->whereIn('service_provider_id', Career::whereHas('specialties', function ($query) use ($specialtyId) {
            return $query->where('id', $specialtyId);
        })
                ->pluck('service_provider_id'));
    }

    protected function specialty_category_id($specialtyCategoryId)
    {
        if ($specialtyCategoryId == 'null') {
            return $this->builder;
        }

        return $this->builder->whereIn('service_provider_id', Career::whereHas('specialties', function ($query) use ($specialtyCategoryId) {
            return $query->where('specialty_category_id', $specialtyCategoryId);
        })
                ->pluck('service_provider_id'));
    }
}
