<?php

namespace App\Database\Connectors;

use COM;

class VFPConnector
{
    public $connection;

    public function __construct()
    {
        $this->connection = new COM('ADODB.Connection', null, CP_UTF8);

        try {
            $this->connection->Open('Provider=VFPOLEDB.1;DSN=ArrowWeb;Mode=ReadWrite;Password="";SourceType=DBC;Collating Sequence=machine;Data Source="'.config('database.connections.sai.database').'";');

            if (!$this->connection) {
                throw new Exception('Could not connect!');
            }
        } catch (Exception $e) {
            echo 'Error (File:): '.$e->getMessage().'<br>';
        }

        if (!$this->connection) {
            exit("Connection Failed: {$this->connection}");
        }
    }

    public function getConnection()
    {
        if (!$this->connection) {
            echo 'Cannot connect, did you initialize the class FVPConnection?';

            return null;
        }

        return $this->connection;
    }
}
