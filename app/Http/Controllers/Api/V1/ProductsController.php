<?php

namespace App\Http\Controllers\Api\V1;

use App\Database\Connectors\VFPConnector;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    public function index()
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto'));
        $data = getTableData($conn, $sql);

        return $data;
    }

    public function show($id)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $product = new Fluent($record);

        // $sql = dump_sql(DB::table('existe')->where('cve_prod', $id));
        // $data = getTableData($conn, $sql);
        // $product->existencias = collect($data)->groupBy('lugar');
        $product->existencias = getInventories($conn, $id);

        // Log::debug("Existencia Producto {$id}, Color: 30/036", [getInventory($conn, "MTY", $id, '30/036')]);

        foreach ($product->existencias as $key => $existencias) {
            foreach ($existencias as $key_ => $invent) {
                $invent_collect = collect($invent);
                $estimated_date = "";

                if ($key == "POR LLEGAR") {
                    $sql_ = dump_sql(DB::table('movs')->where('cve_prod', $id)->where('lugar', $key)->where('new_med', $invent_collect['new_med'])->orderBy('no_mov', 'DESC')->select('no_ref'));
                    $data_ = getTableData($conn, $sql_);
                    $record_ = Arr::first($data_);
                    if (!is_null($record_)) {
                        $mov = new Fluent($record_);
                        if (!empty($mov->no_ref) && Str::contains($mov->no_ref, '/')) {
                            $estimated_date = $mov->no_ref;
                        }
                    }
                }

                $invent_collect->put("estimated_date", $estimated_date);
                $product->existencias[$key][$key_] = $invent_collect;
            }
        }

        $sql = dump_sql(DB::table('prodmed')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);
        $product->precios = $data;

        return $product;
    }

    public function showAllWithInventory()
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto')->select('cve_prod'));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $products = collect($data);

        $productsWithInventory = collect([]);

        foreach ($products as $product_) {
            $product = new Fluent($product_);
            $product->cve_prod = $product->cve_prod;

            $sql = dump_sql(DB::table('existe')->where('cve_prod', $product->cve_prod));
            $data = getTableData($conn, $sql);
            $product->existencias = collect($data)->groupBy('lugar');

            foreach ($product->existencias as $key => $existencias) {
                foreach ($existencias as $key_ => $invent) {
                    $invent_collect = collect($invent);
                    $estimated_date = "";

                    if ($key == "POR LLEGAR") {
                        $sql_ = dump_sql(DB::table('movs')->where('cve_prod', $product->cve_prod)->where('lugar', $key)->where('new_med', $invent_collect['new_med'])->orderBy('no_mov', 'DESC')->select('no_ref'));
                        $data_ = getTableData($conn, $sql_);
                        $record_ = Arr::first($data_);
                        if (!is_null($record_)) {
                            $mov = new Fluent($record_);
                            if (!empty($mov->no_ref) && Str::contains($mov->no_ref, '/')) {
                                $estimated_date = $mov->no_ref;
                            }
                        }
                    }

                    $invent_collect->put("estimated_date", $estimated_date);
                    $product->existencias[$key][$key_] = $invent_collect;
                }
            }

            $productsWithInventory->push($product);
        }

        return $productsWithInventory;
    }

    public function showWithInventory($id, $model)
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('producto')->where('cve_prod', $id));
        $data = getTableData($conn, $sql);

        if (is_null($record = Arr::first($data))) {
            return response('', 404);
        }

        $product = new Fluent($record);

        $sql = dump_sql(DB::table('existe')->where('cve_prod', $id)->where('new_med', 'like', "%{$model}%"));
        $data = getTableData($conn, $sql);
        $product->existencias = collect($data)->groupBy('lugar');

        foreach ($product->existencias as $key => $existencias) {
            foreach ($existencias as $key_ => $invent) {
                $invent_collect = collect($invent);
                $estimated_date = "";

                if ($key == "POR LLEGAR") {
                    $sql_ = dump_sql(DB::table('movs')->where('cve_prod', $id)->where('lugar', $key)->where('new_med', $invent_collect['new_med'])->orderBy('no_mov', 'DESC')->select('no_ref'));
                    $data_ = getTableData($conn, $sql_);
                    $record_ = Arr::first($data_);
                    if (!is_null($record_)) {
                        $mov = new Fluent($record_);
                        if (!empty($mov->no_ref) && Str::contains($mov->no_ref, '/')) {
                            $estimated_date = $mov->no_ref;
                        }
                    }
                }

                $invent_collect->put("estimated_date", $estimated_date);
                $product->existencias[$key][$key_] = $invent_collect;
            }
        }

        $sql = dump_sql(DB::table('prodmed')->where('cve_prod', $id)->where('new_med', 'like', "%{$model}%"));
        $data = getTableData($conn, $sql);
        $product->precios = $data;

        return $product;
    }
}
