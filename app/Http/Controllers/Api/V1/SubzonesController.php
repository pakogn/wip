<?php

namespace App\Http\Controllers\Api\V1;

use App\Database\Connectors\VFPConnector;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SubzonesController extends Controller
{
    public function index()
    {
        $con = new VFPConnector();
        $conn = $con->getConnection();

        $sql = dump_sql(DB::table('subzonas'));
        $data = getTableData($conn, $sql);

        return $data;
    }
}
