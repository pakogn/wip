<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

function getTableData($connection, $sql)
{
    $results = $connection->Execute($sql);
    $payload = [];
    $columnsCount = $results->Fields->Count;

    if ($columnsCount > 0) {
        $columnIndex = 0;

        while (!$results->EOF) {
            for ($index = 0; $index < $columnsCount; $index++) {
                $payload[$columnIndex][$results->Fields($index)->Name] = trim(utf8_encode($results->Fields($index)));
            }

            $columnIndex++;

            $results->MoveNext();
        }
    }

    $results->Close();

    return $payload;
}

function dump_sql($builder)
{
    $sql = $builder->toSql();
    $bindings = $builder->getBindings();

    array_walk($bindings, function ($value) use (&$sql) {
        $value = is_string($value) ? var_export($value, true) : $value;
        $sql = preg_replace("/\?/", $value, $sql, 1);
    });

    return $sql;
}

function dump_command_sql($builder, $command, $bindings = [])
{
    $method = "compile{$command}";
    $sql = $builder->getGrammar()->{$method}($builder, $bindings);

    array_walk($bindings, function ($value) use (&$sql) {
        if (is_string($value)) {
            if (!empty($value) && $value[0] == '\\') {
                $value = Str::after($value, '\\');
            } else {
                $value = var_export($value, true);
            }
        }

        $sql = preg_replace("/\?/", $value, $sql, 1);
    });

    return $sql;
}

function dump_delete_sql($builder)
{
    $sql = $builder->getGrammar()->compileDelete($builder);

    return $sql;
}

function getNextId($conn, $idColumn, $table)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 {$idColumn} from {$table} order by {$idColumn} desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();
    return (string) $lastId;
}

function getNextConfigId($conn, $idColumn, $key)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 {$idColumn} from config where clave = '{$key}' order by clave desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getNextNumPol($conn)
{
    return getNextConfigId($conn, "no_mov", "NUMPOL");
}

function setNextNumPol($conn, $value)
{
    return setNextConfigId($conn, "NUMPOL", $value);
}

function getNextUnicoConfigId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select count(*) from config;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function setNextConfigId($conn, $clave, $no_mov)
{
    $sql = dump_command_sql(DB::table('config')->whereRaw("clave = '{$clave}'"), 'Update', [
        'no_mov' => $no_mov,
    ]);
    $rs = $conn->Execute($sql);

    return true;
}

function getInventory($conn, $lugar, $cve_prod, $new_med)
{
    $sql = dump_sql(DB::table('existe')->where('lugar', $lugar)->where('cve_prod', $cve_prod)->where('new_med', $new_med)->selectRaw("top 1 `existencia`")->orderByDesc("fech_umod"));
    $data = getTableData($conn, $sql);
    // Log::debug("GET INVENTORY DATA", [$sql, $data]);
    if (is_null($record = Arr::first($data))) {
        return null;
    } else {
        $product = new Fluent($record);
        return $product->existencia;
    }
}

function getInventories($conn, $cve_prod)
{
    $sql = dump_sql(DB::table('existe')->where('cve_prod', $cve_prod));
    $data = getTableData($conn, $sql);
    return collect($data)->groupBy('lugar');
}

function incrementInventory($conn, $lugar, $cve_prod, $new_med, $quantity)
{
    $existencia = getInventory($conn, $lugar, $cve_prod, $new_med);

    $newExistencia = $existencia + $quantity;

    $sql = dump_command_sql(DB::table('existe')->whereRaw("lugar = '{$lugar}'")->whereRaw("cve_prod = '{$cve_prod}'")->whereRaw("new_med = '{$new_med}'"), 'Update', [
        'existencia' => floatval($newExistencia),
        'fech_umod' => "\\CTOD('".date("m-d-Y")."')",
    ]);
    $rs = $conn->Execute($sql);

    return true;
}

function decrementInventory($conn, $lugar, $cve_prod, $new_med, $quantity)
{
    $existencia = getInventory($conn, $lugar, $cve_prod, $new_med);

    // Log::debug("RESPUESTA EXISTENCIA Y CANTIDAD", [$existencia, $quantity]);

    $newExistencia = $existencia - $quantity;
    // Log::debug("NEW EXISTENCIA", [$newExistencia]);

    $sql = dump_command_sql(DB::table('existe')->whereRaw("lugar = '{$lugar}'")->whereRaw("cve_prod = '{$cve_prod}'")->whereRaw("new_med = '{$new_med}'"), 'Update', [
        'existencia' => floatval($newExistencia),
        'fech_umod' => "\\CTOD('".date("m-d-Y")."')",
    ]);
    $rs = $conn->Execute($sql);

    // Log::debug("RESULTADO DECREMENT INVENTORY", [$sql, $rs]);

    return true;
}

function insertMovs($conn, $data)
{
    $lastMovsUniqueId = (int) getNextUnicoConfigId($conn);

    $record = array();

    $record['no_mov'] = $data['no_mov']; //consecutivo pero se repite por pedido
    $record['f_mov'] = "\\CTOD('".date("m-d-Y")."')"; //fecha del pedido
    $record['cve_prod'] = $data['cve_prod']; //cve_prod
    $record['no_ref'] = strval($data['no_ref']); //numero pedido
    $record['cant_prod'] = $data['cant_prod']; //cantidad de producto
    $record['costo_ent'] = $data['costo_ent']; //precio de articulo
    $record['cto_prod'] = $data['cto_prod']; //igual
    $record['preci_prod'] = $data['preci_prod']; //precio lista 1 o 2
    $record['cve_age'] = $data['cve_age']; //agente que coloca ped
    $record['cve_cte'] = $data['cve_cte']; //cliente
    $record['new_med'] = $data['new_med']; //codigo del color del producto
    $record['falta_fac'] = "\\CTOD('".date("m-d-Y")."')"; //fecha del pedido

    $record['tipo_mov'] = $data['tipo_mov']; //char 1 S salida, E entrada
    $record['ref_mov'] = ""; //char 3
    $record['med_prod'] = 0; //numeric 5
    $record['orig_mov'] = isset($data['orig_mov']) ? $data['orig_mov'] : "MTY"; //char 10
    $record['dest_mov'] = isset($data['dest_mov']) ? $data['dest_mov'] : ""; //char 10
    $record['cve_prov'] = 0; //numeric 5
    $record['lugar'] = "MTY"; //char 10
    $record['cve_mov'] = $data['cve_mov']; //char 5
    $record['obs'] = ""; //char 1
    $record['hora_mov'] = date("H:i:s"); //char 10
    $record['usuario'] = intval($data['usuario']); //numeric 10 -- 98 DEV y 101 PROD
    $record['cve_mon'] = 1; //numeric 10
    $record['tip_cam'] = 2; //numeric 10
    $record['cve_suc'] = "MAT"; //char 3
    $record['mes'] = date("m"); //char 2
    $record['año'] = date("Y"); //char 4
    $record['trans'] = 1; //numeric 1
    $record['lote'] = ""; //char 30
    $record['usaalm'] = 0; //numeric 1
    $record['cierre'] = ""; //char 1
    $record['cve_cen'] = 0; //numeric 5
    $record['prioridad'] = 5; //numeric 2
    $record['no_pedc'] = 0; //numeric
    $record['suc_ent'] = ""; //char 3
    $record['no_req'] = 0; //numeric
    $record['numpol'] = isset($data['numpol']) ? $data['numpol'] : 0; //numeric
    $record['partida'] = $data['partida']; //numeric 6 Consecutivo no. de partida
    $record['parordcom'] = 0; //numeric 6
    $record['diftoleran'] = 0; //numeric
    $record['movcan'] = 0; //numeric
    $record['succan'] = ""; //char 3
    $record['ref_lote'] = ""; //char
    $record['cto_prodt'] = 0; //numeric
    $record['cve_prodk'] = ""; //char
    $record['part_kit'] = 0; //numeric 6
    $record['uniusu'] = ""; //char 5
    $record['facusu'] = 0; //char 12
    $record['canusu'] = 0; //numeric
    $record['cenusu'] = 0; //numeric
    $record['cprusu'] = 0; //numeric
    $record['desc1'] = 0; //numeric
    $record['desc2'] = 0; //numeric
    $record['ctoentcom'] = 0; //numeric
    $record['dtoentcom'] = 0; //numeric
    $record['ordrefl'] = ""; //char 10
    $record['turno'] = ""; //char 3
    $record['datoest3'] = ""; //char 5
    $record['saldo'] = $data['cant_prod']; //numeric
    $record['no_rem'] = $data['no_rem']; //numeric
    $record['suc_rem'] = "MAT"; //char 3
    $record['lugarinter'] = ""; //char 10
    $record['cve_comp'] = ""; //char 10
    $record['gastotrado'] = 0; //numeric
    $record['salduepeps'] = 0; //numeric
    $record['partespe'] = 0; //numeric
    $record['sincompra'] = ""; //char 1
    $record['t_prorr'] = 0; //numeric
    $record['prorr'] = 0; //numeric 1
    $record['forparic'] = 0; //numeric 1
    $record['fech_lote'] = "\\CTOD('".date("m-d-Y")."')"; //date 8
    $record['folio_srv'] = 0; //numeric 10
    $record['suc_srv'] = ""; //char 3
    $record['iepsec'] = 0; //numeric 5
    $record['ivaec'] = 0; //numeric 5
    $record['retisrec'] = 0; //numeric
    $record['retivaec'] = 0; //numeric
    $record['calivaec'] = ""; //char 4
    $record['id_unico'] = ($lastMovsUniqueId + 1); //numeric
    $record['pmovs'] = 0; //numeric 1
    $record['factorieps'] = 0; //numeric
    $record['iva_ieps'] = 0; //numeric 1
    $record['porcenieps'] = 0; //numeric 5
    $record['ieps_prod'] = 0; //numeric
    $record['fol_stran'] = 0; //numeric
    $record['suc_stran'] = ""; //char 3
    $record['partstran'] = 0; //numeric 6
    $record['nmretot'] = ""; //char 1
    $record['pimpto1'] = 0; //numeric
    $record['pimpto2'] = 0; //numeric
    $record['impto1d'] = 0; //numeric
    $record['impto2d'] = 0; //numeric

    $sql = dump_command_sql(DB::table('movs'), 'Insert', $record);

    // Log::debug("DATA insert movs", [$data]);
    // Log::debug("RECORD insert movs", [$record]);
    // Log::debug("SQL insert movs", [$sql]);

    $rs = $conn->Execute($sql);

    // $sql = "Insert into movs(no_mov, f_mov, cve_prod, ref_mov, no_ref, cant_prod, costo_ent,
    // cto_prod, preci_prod, tipo_mov, med_prod, orig_mov, dest_mov, cve_prov, cve_age, cve_cte,
    // lugar, cve_mov, obs, hora_mov, usuario, cve_mon, tip_cam, cve_suc, mes, año, trans,
    // lote, usaalm, cierre, cve_cen, prioridad, no_pedc, suc_ent, no_req, numpol, partida, parordcom,
    // diftoleran, movcan, succan, ref_lote, cto_prodt, cve_prodk, part_kit, uniusu, facusu, canusu,
    // cenusu, cprusu, desc1, desc2, ctoentcom, dtoentcom, ordrefl, turno, datoest3, saldo, no_rem,
    // suc_rem, lugarinter, new_med, cve_comp, gastotrado, salduepeps, partespe, sincompra, t_prorr,
    // prorr, falta_fac, forparic, fech_lote, folio_srv, suc_srv, iepsec, ivaec, retisrec, retivaec,
    // calivaec, id_unico, pmovs, factorieps, iva_ieps, porcenieps, ieps_prod, fol_stran, suc_stran,
    // partstran, nmretot, pimpto1, pimpto2, impto1d, impto2d) VALUES(" .
    //     $record['no_mov']       . "," .
    //     $record['f_mov']        . "," .
    //     $record['cve_prod']     . "," .
    //     $record['ref_mov']      . "," .
    //     $record['no_ref']       . "," .
    //     $record['cant_prod']    . "," .
    //     $record['costo_ent']    . "," .
    //     $record['cto_prod']     . "," .
    //     $record['preci_prod']   . "," .
    //     $record['tipo_mov']     . "," .
    //     $record['med_prod']     . "," .
    //     $record['orig_mov']     . "," .
    //     $record['dest_mov']     . "," .
    //     $record['cve_prov']     . "," .
    //     $record['cve_age']      . "," .
    //     $record['cve_cte']      . "," .
    //     $record['lugar']        . "," .
    //     $record['cve_mov']      . "," .
    //     $record['obs']          . "," .
    //     $record['hora_mov']     . "," .
    //     $record['usuario']      . "," .
    //     $record['cve_mon']      . "," .
    //     $record['tip_cam']      . "," .
    //     $record['cve_suc']      . "," .
    //     $record['mes']          . "," .
    //     $record['año']          . "," .
    //     $record['trans']        . "," .
    //     $record['lote']         . "," .
    //     $record['usaalm']       . "," .
    //     $record['cierre']       . "," .
    //     $record['cve_cen']      . "," .
    //     $record['prioridad']    . "," .
    //     $record['no_pedc']      . "," .
    //     $record['suc_ent']      . "," .
    //     $record['no_req']       . "," .
    //     $record['numpol']       . "," .
    //     $record['partida']      . "," .
    //     $record['parordcom']    . "," .
    //     $record['diftoleran']   . "," .
    //     $record['movcan']       . "," .
    //     $record['succan']       . "," .
    //     $record['ref_lote']     . "," .
    //     $record['cto_prodt']    . "," .
    //     $record['cve_prodk']    . "," .
    //     $record['part_kit']     . "," .
    //     $record['uniusu']       . "," .
    //     $record['facusu']       . "," .
    //     $record['canusu']       . "," .
    //     $record['cenusu']       . "," .
    //     $record['cprusu']       . "," .
    //     $record['desc1']        . "," .
    //     $record['desc2']        . "," .
    //     $record['ctoentcom']    . "," .
    //     $record['dtoentcom']    . "," .
    //     $record['ordrefl']      . "," .
    //     $record['turno']        . "," .
    //     $record['datoest3']     . "," .
    //     $record['saldo']        . "," .
    //     $record['no_rem']       . "," .
    //     $record['suc_rem']      . "," .
    //     $record['lugarinter']   . "," .
    //     $record['new_med']      . "," .
    //     $record['cve_comp']     . "," .
    //     $record['gastotrado']   . "," .
    //     $record['salduepeps']   . "," .
    //     $record['partespe']     . "," .
    //     $record['sincompra']    . "," .
    //     $record['t_prorr']      . "," .
    //     $record['prorr']        . "," .
    //     $record['falta_fac']    . "," .
    //     $record['forparic']     . "," .
    //     $record['fech_lote']    . "," .
    //     $record['folio_srv']    . "," .
    //     $record['suc_srv']      . "," .
    //     $record['iepsec']       . "," .
    //     $record['ivaec']        . "," .
    //     $record['retisrec']     . "," .
    //     $record['retivaec']     . "," .
    //     $record['calivaec']     . "," .
    //     $record['id_unico']     . "," .
    //     $record['pmovs']        . "," .
    //     $record['factorieps']   . "," .
    //     $record['iva_ieps']     . "," .
    //     $record['porcenieps']   . "," .
    //     $record['ieps_prod']    . "," .
    //     $record['fol_stran']    . "," .
    //     $record['suc_stran']    . "," .
    //     $record['partstran']    . "," .
    //     $record['nmretot']      . "," .
    //     $record['pimpto1']      . "," .
    //     $record['pimpto2']      . "," .
    //     $record['impto1d']      . "," .
    //     $record['impto2d']      . ")";
    // // die($sql);
    // $rs = $conn->Execute($sql);
}

function insertPedidoc($conn, $data)
{
    $record = array();
    $record['no_ped'] = $data['no_ped']; //Numeric
    $record['cve_cte'] = $data['cve_cte']; //Numeric
    $record['cve_age'] = $data['cve_age']; //Numeric
    $record['f_alta_ped'] = "CTOD('".date("m-d-Y")."')";
    $record['descuento'] = $data['descuento']; //Numeric
    $record['descue'] = $data['descue']; //Numeric
    $record['status'] = "'Por Surtir'"; //Character
    $record['total_ped'] = $data['total_ped']; //Numeric
    $record['impr_ped'] = 0; //Numeric
    $record['subt_ped'] = $data['subt_ped']; //Numeric

    $record['iva'] = $data['iva']; //Numeric
    $record['observa'] = str_replace(array("\r\n", "\r", "\n"), ', ', trim($data['observa'])); //Memo 4
    $record['cve_mon'] = 1; //Numeric
    $record['tip_cam'] = 2; //Numeric
    $record['cve_suc'] = $data['cve_suc']; //Character 3
    $record['mes'] = "'".date("m")."'";
    $record['año'] = "'".date("Y")."'";
    $record['usuario'] = intval($data['usuario']); //Numeric 98 DEV y 101 PROD
    $record['trans'] = $data['trans']; //Numeric
    $record['fecha_ent'] = "CTOD('')"; //Date 8

    $record['cierre'] = "''"; //Character 1
    $record['dato_1'] = "''"; //Character 30
    $record['dato_2'] = "''"; //Character 30
    $record['dato_3'] = "''"; //Character 30
    $record['dato_4'] = "''"; //Character 30
    $record['dato_5'] = "''"; //Character 30
    $record['dato_6'] = "''"; //Character 30
    $record['dato_7'] = "''"; //Character 30
    $record['dato_8'] = "''"; //Character 30
    $record['status2'] = "'Confirmado'"; //Character 15

    $record['ped_int'] = "''"; //Character 18
    $record['lugar'] = $data['lugar']; //Character 10
    $record['no_cot'] = 0; //Numeric
    $record['suc_cot'] = "''"; //Character 3
    $record['stat_pro'] = "''"; //Character 10
    $record['usu_auto'] = intval($data['usuario']); //Numeric 98 DEV y 101 PROD
    $record['pesotot'] = 0; //Numeric
    $record['cvedirent'] = "''"; //Character 6
    $record['p_y_d'] = 0; //Logical .F. .T.
    $record['descue2'] = 0; //Numeric

    $record['descue3'] = 0; //Numeric
    $record['descue4'] = 0; //Numeric
    $record['saldoanti'] = 0; //Numeric
    $record['pedref'] = "''"; //Character 1
    $record['cve_entre'] = "'0'"; //Character 5
    $record['ieps'] = 0; //Numeric
    $record['retivaped'] = 0; //Numeric
    $record['no_req'] = 0; //Numeric
    $record['suc_req'] = "''"; //Character 3
    $record['calage1'] = 1; //Numeric

    $record['calage2'] = 0; //Numeric
    $record['cve_age2'] = 0; //Numeric
    $record['cve_monm'] = 0; //Numeric
    $record['hora_ped'] = "'".date("H:i:s A")."'"; //Character 8
    $record['sig_aut'] = 0; //Numeric
    $record['tcdcto1p'] = 1; //Numeric
    $record['tcdcto2p'] = 0; //Numeric
    $record['comori'] = 1; //Numeric
    $record['retisrped'] = 0; //Numeric
    $record['cvede1'] = 0; //Numeric

    $record['cvede2'] = 0; //Numeric
    $record['cvede3'] = 0; //Numeric
    $record['cvede4'] = 0; //Numeric
    $record['cvede5'] = 0; //Numeric
    $record['cvede6'] = 0; //Numeric
    $record['cvede7'] = 0; //Numeric
    $record['cvede8'] = 0; //Numeric
    $record['usadomap'] = 0; //Numeric
    $record['savemovi'] = "''"; //Character 1
    $record['mrop'] = 0; //Numeric

    $record['folio'] = 0; //Numeric
    $record['nmretot'] = "''"; //Character 1
    $record['impto1'] = 0; //Numeric
    $record['impto2'] = 0; //Numeric
    $record['timpto1'] = 0; //Numeric
    $record['timpto2'] = 0; //Numeric
    $record['cam_auto'] = 0; //Numeric
    $record['imp1ieps'] = 0; //Numeric SAR 20220209
    $record['imp2ieps'] = 0; //Numeric SAR 20220209

    $sql = "Insert into pedidoc(no_ped, cve_cte, cve_age, f_alta_ped, descuento, descue, status, total_ped, impr_ped, subt_ped,
    iva, observa, cve_mon, tip_cam, cve_suc, mes, año, usuario, trans, fecha_ent, cierre, dato_1, dato_2, dato_3, dato_4, dato_5,
    dato_6, dato_7, dato_8, status2, ped_int, lugar, no_cot, suc_cot, stat_pro, usu_auto, pesotot, cvedirent, p_y_d, descue2,
    descue3, descue4, saldoanti, pedref, cve_entre, ieps, retivaped, no_req, suc_req, calage1, calage2, cve_age2, cve_monm,
    hora_ped, sig_aut, tcdcto1p, tcdcto2p, comori, retisrped, cvede1, cvede2, cvede3, cvede4, cvede5, cvede6, cvede7, cvede8,
    usadomap, savemovi, mrop, folio, nmretot, impto1, impto2, timpto1, timpto2, cam_auto, imp1ieps, imp2ieps) VALUES(".

    $record['no_ped'].",".
    $record['cve_cte'].",".
    $record['cve_age'].",".
    $record['f_alta_ped'].",".
    $record['descuento'].",".
    $record['descue'].",".
    $record['status'].",".
    $record['total_ped'].",".
    $record['impr_ped'].",".
    $record['subt_ped'].",".

    $record['iva'].",".
    $record['observa'].",".
    $record['cve_mon'].",".
    $record['tip_cam'].",".
    $record['cve_suc'].",".
    $record['mes'].",".
    $record['año'].",".
    $record['usuario'].",".
    $record['trans'].",".
    $record['fecha_ent'].",".

    $record['cierre'].",".
    $record['dato_1'].",".
    $record['dato_2'].",".
    $record['dato_3'].",".
    $record['dato_4'].",".
    $record['dato_5'].",".
    $record['dato_6'].",".
    $record['dato_7'].",".
    $record['dato_8'].",".
    $record['status2'].",".

    $record['ped_int'].",".
    $record['lugar'].",".
    $record['no_cot'].",".
    $record['suc_cot'].",".
    $record['stat_pro'].",".
    $record['usu_auto'].",".
    $record['pesotot'].",".
    $record['cvedirent'].",".
    $record['p_y_d'].",".
    $record['descue2'].",".

    $record['descue3'].",".
    $record['descue4'].",".
    $record['saldoanti'].",".
    $record['pedref'].",".
    $record['cve_entre'].",".
    $record['ieps'].",".
    $record['retivaped'].",".
    $record['no_req'].",".
    $record['suc_req'].",".
    $record['calage1'].",".

    $record['calage2'].",".
    $record['cve_age2'].",".
    $record['cve_monm'].",".
    $record['hora_ped'].",".
    $record['sig_aut'].",".
    $record['tcdcto1p'].",".
    $record['tcdcto2p'].",".
    $record['comori'].",".
    $record['retisrped'].",".
    $record['cvede1'].",".

    $record['cvede2'].",".
    $record['cvede3'].",".
    $record['cvede4'].",".
    $record['cvede5'].",".
    $record['cvede6'].",".
    $record['cvede7'].",".
    $record['cvede8'].",".
    $record['usadomap'].",".
    $record['savemovi'].",".
    $record['mrop'].",".

    $record['folio'].",".
    $record['nmretot'].",".
    $record['impto1'].",".
    $record['impto2'].",".
    $record['timpto1'].",".
    $record['timpto2'].",".
    $record['cam_auto'].",".
    $record['imp1ieps'].",".// SAR 20220209
    $record['imp2ieps'].");"; // SAR 20220209

    // pre($record);
    // echo $sql;
    // die();
    // die();
    // $rs = $conn->Execute("SELECT VERSION() AS vers, ISREADONLY('pedidoc') AS ro,  no_ped FROM pedidoc");
    // pre($rs->Fields(0)->value);
    // pre($rs->Fields(1)->value);
    // pre($rs->Fields(2)->value);
    //$conn->setCharset('utf8');
    $rs = $conn->Execute($sql);
    //$rs = $conn->autoExecute('pedidoc',$record,'INSERT');
    //$rs->Close();
    // echo $rs;
}

function insertPedientr($conn, $data)
{
    $record = array();
    $record['cve_suc'] = $data['cve_suc']; //String
    $record['no_ped'] = $data['no_ped']; //Numeric
    $record['nom_ent'] = str_replace(array("\r\n", "\r", "\n"), ', ', trim($data['nom_ent'])); //String
    $record['dir_ent'] = ""; //String
    $record['col_ent'] = ""; //String
    $record['cd_ent'] = ""; //String
    $record['edo_ent'] = ""; //String
    $record['cp_ent'] = ""; //String
    $record['trans'] = 1; //Numeric
    $record['ne_ent'] = ""; //String
    $record['ni_ent'] = ""; //String
    $record['delega_ent'] = ""; //String
    //$record['delega_ent'] = "''"; //String SAR 20220209
    $record['pais_ent'] = ""; //String
    $record['coor_ent'] = ""; //String
    $record['cve_col'] = ""; //String    SAR 20220209
    $record['cve_mpio'] = ""; //String    SAR 20220209
    $record['cve_local'] = ""; //String    SAR 20220209
    $record['cve_edo'] = ""; //String    SAR 20220209
    $record['resi_ent'] = ""; //String    SAR 20220209
    $record['chk_dir'] = 0; // Numeric     SAR 20220209

    $sql = dump_command_sql(DB::table('pedientr'), 'Insert', $record);

    $rs = $conn->Execute($sql);
}

function insertRementr($conn, $data)
{
    $record = array();
    $record['suc_rem'] = $data['suc_rem']; //String
    $record['no_rem'] = $data['no_rem']; //Numeric
    $record['nom_ent'] = $data['nom_ent']; //String
    $record['dir_ent'] = ""; //String
    $record['col_ent'] = ""; //String
    $record['cd_ent'] = ""; //String
    $record['edo_ent'] = ""; //String
    $record['cp_ent'] = ""; //String
    $record['dato_1'] = ""; //String
    $record['dato_2'] = ""; //String
    $record['dato_3'] = ""; //String
    $record['dato_4'] = ""; //String
    $record['dato_5'] = ""; //String
    $record['trans'] = 1; //Numeric
    $record['dato_6'] = ""; //String
    $record['dato_7'] = ""; //String
    $record['dato_8'] = ""; //String
    $record['ne_ent'] = ""; //String
    $record['ni_ent'] = ""; //String
    $record['delega_ent'] = ""; //String  ''
    //$record['delega_ent'] = ""; String SAR 20220209  ''
    $record['pais_ent'] = ""; //String  ''
    $record['coor_ent'] = ""; //String  ''
    $record['cve_col'] = ""; //String    SAR 20220209  ''
    $record['cve_mpio'] = ""; //String   SAR 20220209  ''
    $record['cve_local'] = ""; //String  SAR 20220209  ''
    $record['cve_edo'] = ""; //String    SAR 20220209  ''
    $record['resi_ent'] = ""; //String   SAR 20220209  ''
    $record['chk_dir'] = 0; //Numeric      SAR 20220209

    $sql = dump_command_sql(DB::table('rementr'), 'Insert', $record);

    $rs = $conn->Execute($sql);
}

function insertPedidod($conn, $data)
{
    $record = array();

    $record['no_ped'] = $data['no_ped']; //Numeric
    $record['cve_prod'] = $data['cve_prod']; //Character
    $record['cse_prod'] = $data['cse_prod']; //Character 10
    $record['med_prod'] = 0; //Numeric
    $record['cant_prod'] = $data['cant_prod']; //Numeric

    $record['valor_prod'] = $data['valor_prod']; //Numeric
    $record['cpm_ped'] = 0; //Numeric
    $record['fecha_ent'] = "\\CTOD('')";
    $record['status1'] = ""; //Character 1
    $record['saldo'] = $data['saldo']; //Numeric

    $record['iva_prod'] = 16; //Numeric
    $record['factor'] = 1; //Numeric
    $record['unidad'] = "PIEZA"; //Character 5
    $record['cve_suc'] = $data['cve_suc']; //Character 3
    $record['trans'] = 1; //Numeric 1

    $record['conv_var'] = 0; //Numeric 1
    $record['dcto1'] = 0; //Numeric
    $record['dcto2'] = 0; //Numeric
    $record['dctotot'] = 0; //Numeric
    $record['lote'] = ""; //Character

    $record['stat_pro'] = ""; //Character 10
    $record['fol_prod'] = $data['fol_prod']; //Numeric
    $record['lista_pre'] = strval($data['lista_pre']); //Character
    $record['cve_mon_d'] = 1; //Numeric
    $record['por_pitex'] = 0; //Numeric

    $record['caliva'] = "TIPO"; //Character 4
    $record['porcretiva'] = 0; //Numeric
    $record['porcenieps'] = 0; //Numeric
    $record['ieps_prod'] = 0; //Numeric
    $record['reten_iva'] = 0; //Numeric

    $record['new_med'] = $data['new_med']; //Character 6
    $record['comage1p'] = 0; //Numeric
    $record['dinage1p'] = 0; //Numeric
    $record['comage2p'] = 0; //Numeric
    $record['dinage2p'] = 0; //Numeric

    $record['prec_ant'] = 0; //Numeric
    $record['dcto1_ant'] = 0; //Numeric
    $record['dcto2_ant'] = 0; //Numeric
    $record['lista_ant'] = ""; //Character 1
    $record['fecha_ant'] = "\\CTOD('')";

    $record['clista1'] = ""; //Character 2
    $record['clista2'] = ""; //Character 2
    $record['retisrpedp'] = 0; //Numeric
    $record['retisrpedd'] = 0; //Numeric
    $record['factorieps'] = 0; //Numeric

    $record['iva_ieps'] = 0; //Numeric
    $record['kpc_doc'] = 0; //Numeric
    $record['no_promo'] = 0; //Numeric
    $record['pimpto1'] = 0; //Numeric
    $record['pimpto2'] = 0; //Numeric

    $record['impto1d'] = 0; //Numeric
    $record['impto2d'] = 0; //Numeric
    $record['facto1ieps'] = 0; //Numeric SAR 20220209
    $record['iva1ieps'] = 0; //Numeric SAR 20220209
    $record['facto2ieps'] = 0; //Numeric SAR 20220209
    $record['iva2ieps'] = 0; //Numeric SAR 20220209

    $sql = dump_command_sql(DB::table('pedidod'), 'Insert', $record);

    // Log::debug("SQL insertPedidod", [$sql]);

    $rs = $conn->Execute($sql);

    // $sql = "Insert into pedidod(no_ped, cve_prod, cse_prod, med_prod, cant_prod, valor_prod, cpm_ped, fecha_ent, status1, saldo,
    // iva_prod,    factor,  unidad,  cve_suc, trans,   conv_var,    dcto1,   dcto2,   dctotot, lote,    stat_pro,    fol_prod,
    // lista_pre,   cve_mon_d,   por_pitex,   caliva,  porcretiva,  porcenieps,  ieps_prod,   reten_iva,   new_med, comage1p, dinage1p,
    // comage2p,    dinage2p,    prec_ant,    dcto1_ant,   dcto2_ant,   lista_ant,   fecha_ant,   clista1, clista2, retisrpedp,
    // retisrpedd,  factorieps,  iva_ieps,    kpc_doc, no_promo,    pimpto1, pimpto2, impto1d, impto2d) values(" .
    //     $record['no_ped'] . "," .
    //     $record['cve_prod'] . "," .
    //     $record['cse_prod'] . "," .
    //     $record['med_prod'] . "," .
    //     $record['cant_prod'] . "," .
    //     $record['valor_prod'] . "," .
    //     $record['cpm_ped'] . "," .
    //     $record['fecha_ent'] . "," .
    //     $record['status1'] . "," .
    //     $record['saldo'] . "," .
    //     $record['iva_prod'] . "," .
    //     $record['factor'] . "," .
    //     $record['unidad'] . "," .
    //     $record['cve_suc'] . "," .
    //     $record['trans'] . "," .
    //     $record['conv_var'] . "," .
    //     $record['dcto1'] . "," .
    //     $record['dcto2'] . "," .
    //     $record['dctotot'] . "," .
    //     $record['lote'] . "," .
    //     $record['stat_pro'] . "," .
    //     $record['fol_prod'] . "," .
    //     $record['lista_pre'] . "," .
    //     $record['cve_mon_d'] . "," .
    //     $record['por_pitex'] . "," .
    //     $record['caliva'] . "," .
    //     $record['porcretiva'] . "," .
    //     $record['porcenieps'] . "," .
    //     $record['ieps_prod'] . "," .
    //     $record['reten_iva'] . "," .
    //     $record['new_med'] . "," .
    //     $record['comage1p'] . "," .
    //     $record['dinage1p'] . "," .
    //     $record['comage2p'] . "," .
    //     $record['dinage2p'] . "," .
    //     $record['prec_ant'] . "," .
    //     $record['dcto1_ant'] . "," .
    //     $record['dcto2_ant'] . "," .
    //     $record['lista_ant'] . "," .
    //     $record['fecha_ant'] . "," .
    //     $record['clista1'] . "," .
    //     $record['clista2'] . "," .
    //     $record['retisrpedp'] . "," .
    //     $record['retisrpedd'] . "," .
    //     $record['factorieps'] . "," .
    //     $record['iva_ieps'] . "," .
    //     $record['kpc_doc'] . "," .
    //     $record['no_promo'] . "," .
    //     $record['pimpto1'] . "," .
    //     $record['pimpto2'] . "," .
    //     $record['impto1d'] . "," .
    //     $record['impto2d'] . ");";
    // $rs = $conn->Execute($sql);
}

function insertRemc($conn, $data)
{
    $record = array();
    $record['no_rem'] = $data['no_rem']; // numeric
    $record['suc_rem'] = $data['cve_suc']; //Character
    $record['cve_cte'] = $data['cve_cte']; //Numeric
    $record['cve_age'] = $data['cve_age']; //Numeric
    $record['falta_rem'] = "\\CTOD('".date("m-d-Y")."')"; // Date
    $record['status_rem'] = "Emitida"; // Character
    $record['observa'] = $data['observa']; //Memo
    $record['lugar'] = $data['lugar']; //Character
    $record['cve_mon'] = 1; //Numeric
    $record['tip_cam'] = 2; //Numeric
    $record['mes'] = date("m"); //Character
    $record['año'] = date("Y"); //Character
    $record['usuario'] = intval($data['usuario']); //Numeric 98 DEV y 101 PROD
    $record['trans'] = $data['trans']; //Numeric

    $record['ped_int'] = "";
    $record['cierre'] = "";
    $record['u_tip_cam'] = 2.0;
    $record['fech_sal'] = "\\CTOD('')";
    // $record['fech_sal'] = "\\CTOD('" . date("m-d-Y") . "')";
    $record['fech_emb'] = "\\CTOD('')";
    // $record['fech_emb'] = "\\CTOD('" . date("m-d-Y") . "')";
    $record['cve_flet'] = "";
    $record['tot_flet'] = 0;
    $record['tot_env'] = 0;
    $record['prec_flet'] = 0;
    $record['prv_real'] = 0;

    $record['descue'] = $data['descue'];
    $record['pesotot'] = 0;
    $record['subt_rem'] = $data['subt_ped'];
    $record['descuento'] = $data['descuento'];
    $record['total_rem'] = $data['total_ped'];
    $record['cvedirent'] = "";

    $record['p_y_d'] = 0;
    $record['cve_entre'] = "0";
    $record['descue2'] = 0;
    $record['descue3'] = 0;
    $record['descue4'] = 0;
    $record['retiva_rem'] = 0;
    $record['ieps'] = 0;
    $record['usu_auto'] = intval($data['usuario']); //Numeric 98 DEV y 101 PROD
    $record['calage1'] = 1;
    $record['calage2'] = 0;
    $record['cve_age2'] = 0;
    $record['cve_monm'] = 0;
    $record['hora_rem'] = date("H:i:s A");

    $record['rtipo'] = "";
    $record['rsubtipo'] = "";
    $record['rporcxtip'] = 0;
    $record['tcdcto1p'] = 1;
    $record['tcdcto2p'] = 0;
    $record['retisrrem'] = 0;

    $record['cvede1'] = 0;
    $record['cvede2'] = 0;
    $record['cvede3'] = 0;
    $record['cvede4'] = 0;
    $record['cvede5'] = 0;
    $record['cvede6'] = 0;
    $record['cvede7'] = 0;
    $record['cvede8'] = 0;

    $record['usadomap'] = 0;
    $record['nmretot'] = "";
    $record['impto1'] = 0;
    $record['impto2'] = 0;
    $record['timpto1'] = 0;
    $record['timpto2'] = 0;
    $record['imp1ieps'] = 0; // SAR 20220209
    $record['imp2ieps'] = 0; // SAR 20220209

    $sql = dump_command_sql(DB::table('remc'), 'Insert', $record);

    $rs = $conn->Execute($sql);

    // $sql = "Insert into remc(no_rem, suc_rem, cve_cte, cve_age, falta_rem, status_rem, observa, lugar, cve_mon, tip_cam, mes, año, usuario, trans, ped_int, cierre, u_tip_cam, fech_sal, fech_emb, cve_flet, tot_flet, tot_env, prec_flet, prv_real, descue, pesotot, subt_rem, descuento, total_rem, cvedirent, p_y_d, cve_entre, descue2, descue3, descue4, retiva_rem, ieps, usu_auto, calage1, calage2, cve_age2, cve_monm, hora_rem, rtipo, rsubtipo, rporcxtip, tcdcto1p, tcdcto2p, retisrrem, cvede1, cvede2, cvede3, cvede4, cvede5, cvede6, cvede7, cvede8, usadomap, nmretot, impto1, impto2, timpto1, timpto2) VALUES(" .

    //     $record['no_rem'] . "," .
    //     $record['suc_rem'] . "," .
    //     $record['cve_cte'] . "," .
    //     $record['cve_age'] . "," .
    //     $record['falta_rem'] . "," .
    //     $record['status_rem'] . "," .
    //     $record['observa'] . "," .
    //     $record['lugar'] . "," .
    //     $record['cve_mon'] . "," .
    //     $record['tip_cam'] . "," .
    //     $record['mes'] . "," .
    //     $record['año'] . "," .
    //     $record['usuario']  . "," .
    //     $record['trans'] . "," .
    //     $record['ped_int'] . "," .
    //     $record['cierre'] . "," .
    //     $record['u_tip_cam'] . "," .
    //     $record['fech_sal'] . "," .
    //     $record['fech_emb'] . "," .
    //     $record['cve_flet'] . "," .
    //     $record['tot_flet'] . "," .
    //     $record['tot_env'] . "," .
    //     $record['prec_flet'] . "," .
    //     $record['prv_real'] . "," .
    //     $record['descue'] . "," .
    //     $record['pesotot'] . "," .
    //     $record['subt_rem'] . "," .
    //     $record['descuento'] . "," .
    //     $record['total_rem'] . "," .
    //     $record['cvedirent'] . "," .
    //     $record['p_y_d'] . "," .
    //     $record['cve_entre'] . "," .
    //     $record['descue2'] . "," .
    //     $record['descue3'] . "," .
    //     $record['descue4'] . "," .
    //     $record['retiva_rem'] . "," .
    //     $record['ieps'] . "," .
    //     $record['usu_auto'] . "," .
    //     $record['calage1'] . "," .
    //     $record['calage2'] . "," .
    //     $record['cve_age2'] . "," .
    //     $record['cve_monm'] . "," .
    //     $record['hora_rem'] . "," .
    //     $record['rtipo'] . "," .
    //     $record['rsubtipo'] . "," .
    //     $record['rporcxtip'] . "," .
    //     $record['tcdcto1p'] . "," .
    //     $record['tcdcto2p'] . "," .
    //     $record['retisrrem'] . "," .
    //     $record['cvede1'] . "," .
    //     $record['cvede2'] . "," .
    //     $record['cvede3'] . "," .
    //     $record['cvede4'] . "," .
    //     $record['cvede5'] . "," .
    //     $record['cvede6'] . "," .
    //     $record['cvede7'] . "," .
    //     $record['cvede8'] . "," .
    //     $record['usadomap'] . "," .
    //     $record['nmretot'] . "," .
    //     $record['impto1'] . "," .
    //     $record['impto2'] . "," .
    //     $record['timpto1'] . "," .
    //     $record['timpto2'] . ");";

    // $rs = $conn->Execute($sql);
}

function insertRemd($conn, $data)
{
    $record = array();
    $record['no_rem'] = $data['no_rem'];
    $record['suc_rem'] = $data['cve_suc'];
    $record['no_ped'] = $data['no_ped'];
    $record['cse_prod'] = $data['cse_prod'];
    $record['cve_prod'] = $data['cve_prod'];

    $record['cant_prod'] = $data['cant_prod'];
    $record['valor_prod'] = $data['valor_prod']; // El precio unitario del producto
    $record['cant_surt'] = $data['cant_prod'];

    $record['cost_prom'] = $data['valor_prod'];
    $record['cost_repo'] = $data['valor_prod'];
    $record['descu_prod'] = $data['descu_prod'];
    $record['subt_prod'] = floatval($data['cant_prod'] * $data['valor_prod']);

    $record['med_prod'] = 0.00;
    $record['med_prodf'] = 0.00;
    $record['iva_prod'] = 16.00;
    $record['factor'] = 1.00;
    $record['unidad'] = "PIEZA";
    $record['cve_suc'] = $data['cve_suc'];
    $record['trans'] = 1;
    $record['lote'] = "";

    $record['cve_env'] = "";
    $record['can_env'] = 0.00;
    $record['val_env'] = 0.00;
    $record['ctopr_env'] = 0.00;
    $record['ctore_env'] = 0.00;
    $record['env_prod'] = 0.00;
    $record['flet_prod'] = 0.00;
    $record['conv_var'] = 0;
    $record['staclas1'] = 0;
    $record['status1'] = "";

    $record['saldo'] = $data['cant_prod'];
    $record['saldo_env'] = 0.00;
    $record['dcto1'] = 0.00;
    $record['dcto2'] = 0.00;
    $record['dctotot'] = 0.00;

    $record['part_rem'] = $data['part_rem'];
    $record['part_ped'] = $data['part_ped'];
    $record['lista_pre'] = $data['lista_pre'];
    $record['por_pitex'] = 0.00;
    $record['cve_mon_d'] = 1;
    $record['caliva'] = "TIPO";

    $record['retiva_pro'] = 0.00;
    $record['porcretiva'] = 0.00;
    $record['ieps_prod'] = 0.00;
    $record['porcenieps'] = 0.00;
    $record['new_med'] = $data['new_med'];

    $record['ref_lote'] = "";
    $record['comage1p'] = 0.00;
    $record['dinage1p'] = 0.00;
    $record['comage2p'] = 0.00;
    $record['dinage2p'] = 0.00;

    $record['clista1'] = "";
    $record['clista2'] = "";
    $record['retisrremp'] = 0.00;
    $record['retisrremd'] = 0.00;
    $record['factorieps'] = 0.00;

    $record['iva_ieps'] = 0;
    $record['kpc_doc'] = 0;
    $record['no_promo'] = 0;
    $record['pimpto1'] = 0.00;

    $record['pimpto2'] = 0.00;
    $record['impto1d'] = 0.00;
    $record['impto2d'] = 0.00;
    $record['facto1ieps'] = 0; // SAR 20220209
    $record['iva1ieps'] = 0; // SAR 20220209
    $record['facto2ieps'] = 0; // SAR 20220209
    $record['iva2ieps'] = 0; // SAR 20220209

    $sql = dump_command_sql(DB::table('remd'), 'Insert', $record);

    // Log::debug("SQL insertRemd", [$sql]);

    $rs = $conn->Execute($sql);
}

// Insertar poliza
function insertPoliza($conn, $data)
{
    // Obtenemos el id de siguiente poliza
    if (isset($data['numpol']) && !empty($data['numpol'])) {
        $numpol = $data['numpol'];
    } else {
        $numpol = getNextNumPol($conn);
    }

    $record = array();

    $record['numpol'] = $numpol;
    $record['conc_pol'] = $data['conc_pol'];
    $record['fecha'] = "\\CTOD('".date("m-d-Y")."')";
    $record['cargos'] = $data['cargos'];
    $record['abonos'] = $data['abonos'];
    $record['sistorig'] = 1;
    $record['cve_mov'] = 0;
    $record['no_cheq'] = 0;
    $record['num_cuenta'] = 0;
    $record['tip_cam'] = 1;
    $record['enviado'] = "F";
    $record['folio_pol'] = 0;
    $record['tipo_pol'] = $data['tipo_pol'];
    $record['no_mov'] = $data['no_mov'];
    $record['suc_mov'] = $data['suc_mov'];
    $record['tippol'] = "D";
    $record['folgentxt'] = 0;
    $record['mes'] = date("m");
    $record['año'] = date("Y");
    $record['estatus'] = "Emitida";
    $record['usuarioc'] = 0;
    $record['fechac'] = "\\CTOD('01-01-2000')";
    $record['horac'] = "";
    $record['polimod'] = 0;
    $record['cve_mon'] = 1;
    $record['usuario'] = intval($data['usuario']);
    $record['hora'] = date("H:i:s");
    $record['cierree'] = 0;
    $record['estatxml'] = 0;
    $record['numpolsat'] = 0;
    $record['cvesal'] = 0; // SAR 20220209
    $record['sucsal'] = ""; // SAR 20220209 ''

    $sql = dump_command_sql(DB::table('polizas'), 'Insert', $record);

    // Log::debug("Polizas record", [$record]);
    // Log::debug("Polizas sql", [$sql]);

    $rs = $conn->Execute($sql);

    // Al final seteamos el siguiente id en config
    setNextNumPol($conn, ($numpol + 1));
}

// Insertar folio de pedido
function insertFolPedVe($conn, $data)
{
    $record = array();

    $record['cve_suc'] = "MAT";
    $record['no_ped'] = $data['no_ped'];
    $record['año'] = date("Y");
    $record['mes'] = date("m");
    $record['trans'] = 0;
    $record['f_alta_ped'] = "\\CTOD('".date("m-d-Y")."')";
    $record['cve_age'] = intval($data['cve_age']);
    $record['lugar'] = "MTY";
    $record['cve_cte'] = intval($data['cve_cte']);

    $sql = dump_command_sql(DB::table('folpedve'), 'Insert', $record);

    // Log::debug("Polizas record", [$record]);
    // Log::debug("Polizas sql", [$sql]);

    $rs = $conn->Execute($sql);
}

// Insertar folio de remision
function insertFolRemVe($conn, $data)
{
    $record = array();

    $record['suc_rem'] = "MAT";
    $record['no_rem'] = $data['no_rem'];
    $record['año'] = date("Y");
    $record['mes'] = date("m");
    $record['trans'] = 1;
    $record['falta_rem'] = "\\CTOD('".date("m-d-Y")."')";
    $record['cve_age'] = intval($data['cve_age']);
    $record['lugar'] = "MTY";
    $record['cve_cte'] = intval($data['cve_cte']);

    $sql = dump_command_sql(DB::table('folremve'), 'Insert', $record);

    // Log::debug("Polizas record", [$record]);
    // Log::debug("Polizas sql", [$sql]);

    $rs = $conn->Execute($sql);
}

function validateParams($allPostPutVars)
{
    $cve_cte = isset($allPostPutVars['data']['cve_cte']) ? $allPostPutVars['data']['cve_cte'] : null;
    $cve_age = isset($allPostPutVars['data']['cve_age']) ? $allPostPutVars['data']['cve_age'] : null;
    $pedidos = isset($allPostPutVars['data']['pedidos']) ? $allPostPutVars['data']['pedidos'] : null;
    if (!($cve_cte && is_numeric($cve_cte))) {
        return "Cve_cte incorrecto";
    }
    if (!($cve_age && is_numeric($cve_age))) {
        return "Cve_age incorrecto";
    }
    $i = 1;
    if (count($pedidos) > 0) {
        foreach ($pedidos as $pedido) {
            if (!strlen($pedido['cve_prod']) > 0) {
                return "cve_prod del pedido #".$i." incorrecto";
            }
            if (!($pedido['cantidad'] > 0 && is_numeric($pedido['cantidad']))) {
                return "cantidad del pedido #".$i." incorrecto";
            }
            $i++;
        }
    } else {
        return "no hay pedidos";
    }
    return "OK";
}

function getNextPedidocId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 no_ped from pedidoc order by no_ped desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getNextRemcId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 no_rem from remc order by no_rem desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getNextMovsId($conn)
{
    $lastId = "";
    $rs = $conn->Execute("select top 1 no_mov from movs order by no_mov desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();

    return (string) $lastId;
}

function getFirst($connection, $table, $column)
{
    $results = $connection->Execute("select top 1 * from {$table} order by {$column} desc;");
    $payload = [];
    $columnsCount = $results->Fields->Count;

    if ($columnsCount > 0) {
        $columnIndex = 0;

        while (!$results->EOF) {
            for ($index = 0; $index < $columnsCount; $index++) {
                $payload[$columnIndex][$results->Fields($index)->Name] = trim(utf8_encode($results->Fields($index)));
            }

            $columnIndex++;

            $results->MoveNext();
        }
    }

    $results->Close();

    return $payload;
}
