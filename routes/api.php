<?php

use App\Database\Connectors\VFPConnector;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Fluent;

$saiTables = ['agentes', 'AGRSAT', 'agru_est', 'agructos', 'agrupacot', 'agrupad', 'agrupcot', 'akit_nvt', 'ALMADERE', 'almsfrxs', 'analisis', 'anpagos2', 'anventac', 'anventad', 'arbolc', 'arch_xls', 'asegchp', 'asigreac', 'asigread', 'atributc', 'ATRIBUTO', 'auto_oc', 'AUTO_PED', 'auto_req', 'bancossat', 'bitacfds', 'bitecom', 'bitecta', 'biteempl', 'boletad', 'boletas', 'borra', 'buscador', 'busqnomv', 'calapis', 'calenlac', 'calevent', 'calexcep', 'camion', 'canald', 'caracpcon', 'caracprd', 'carentr', 'cargodoc', 'cargoxm2', 'cargoxml', 'carinico', 'carinive', 'carpdoc', 'cat_con', 'cat_est', 'cat_gpo', 'cat_map', 'cataenti', 'catasatc', 'catasatd', 'catentr2', 'CATENTRE', 'catetrab', 'causas', 'ccdenomi', 'ccretiro', 'cencosal', 'centcost', 'certcacp', 'cfd', 'cfdirelac', 'cfgrepbs', 'CFGREPCA', 'CFGREPEN', 'cfgrepor', 'CFGREPTA', 'cfgrepwh', 'checcfdi', 'cheq', 'chklistc', 'chklistp', 'chklistv', 'chofer', 'cierdme2', 'cierdmes', 'clases', 'clasific', 'clieimag', 'CLIENTES', 'clienxml', 'cnfgdos', 'cnfpd_vt', 'coagxcon', 'COAGXCTE', 'coagxdef', 'coagxfac', 'coagxli2', 'coagxlis', 'COAGXPED', 'coagxpro', 'coagxrem', 'codbarcf', 'COLORNTA', 'colortot', 'COM_AGE', 'comagcl', 'comagpd', 'compdoc', 'compradc', 'comprafc', 'comprafd', 'comprapc', 'COMPRAPD', 'comproc', 'comprod', 'con_cve', 'con_est', 'con_fol', 'concc', 'concd', 'conceptos', 'concoc', 'concod', 'concp', 'concr', 'conddeta', 'conddocu', 'condentr', 'confcont', 'confdeca', 'confdeec', 'conficpbi', 'confiecom', 'config', 'config1', 'CONFIMP2', 'confimpo', 'conflote', 'consdeta', 'consdocu', 'consemp', 'consentr', 'consignc', 'consignd', 'conspoli', 'CONTACTO', 'CONTADO', 'contdmes', 'contened', 'contpro', 'CONTPROS', 'controle', 'controlp', 'cortcajc', 'cortcajd', 'costeoc', 'costeod', 'costeos', 'cotcdeta', 'cotcdoc', 'cotcompc', 'cotcompd', 'COTIDETA', 'cotidoc', 'cotiespc', 'cotiespd', 'cotizac', 'COTIZAD', 'cotper', 'cred2d', 'credidoc', 'credito2', 'creditod', 'creditos', 'credixm2', 'credixml', 'credxmld', 'creinico', 'creinive', 'crepdoc', 'cta_conc', 'cta_ctes', 'cta_imp', 'cta_proc', 'cta_proc2', 'cta_proc3', 'cta_prod', 'cta_prop', 'ctadepto', 'ctaplazo', 'ctas_aso', 'ctas_ext', 'ctasclie', 'ctascont', 'ctasempl', 'ctasgen', 'ctasprov', 'ctativac', 'ctativap', 'ctecargo', 'CTECLABE', 'ctecon', 'ctecot', 'ctecredi', 'ctedcon', 'ctedir', 'ctefac', 'cteped', 'cterem', 'ctesfrxs', 'ctexclas', 'CTEXPROD', 'cuentas', 'cuentref', 'cvconv', 'cvconvc', 'cvconvi', 'cvconvp', 'cveanti', 'cvetras', 'cvpoli', 'cvresp', 'cvtemas', 'cvtemasp', 'daestag1', 'daestag2', 'daestag3', 'daestag4', 'daestal1', 'daestal2', 'daestal3', 'daestal4', 'daestcl1', 'daestcl2', 'daestcl3', 'daestcl4', 'daestdc', 'daestdc1', 'daestdc2', 'daestdc3', 'daestdc4', 'daestdc5', 'daestdc6', 'daestdc7', 'daestdc8', 'daestem1', 'daestem2', 'daestem3', 'daestem4', 'daestpd1', 'daestpd2', 'daestpd3', 'daestpd4', 'daestpv1', 'daestpv2', 'daestpv3', 'daestpv4', 'dcagxcon', 'dcagxfac', 'dcagxped', 'dcagxrem', 'dctocoag', 'ddacon', 'ddadeta', 'ddadocu', 'ddaentr', 'ddpdeta', 'denbill', 'denmone', 'departa', 'deposic', 'deposid', 'DERECHOS', 'derepend', 'dermovil', 'des_etiq', 'descxalm', 'destdir', 'devconsc', 'devconsd', 'diascli', 'doc_comc', 'doc_comd', 'doc_coml', 'docscte', 'docsempl', 'docsprod', 'docspros', 'docsprov', 'ecommer', 'emailsai', 'embarcar', 'empleados', 'empresas', 'equipfot', 'equipos', 'estadcat', 'estadpro', 'estcdeta', 'estcdoc', 'estimac', 'estimad', 'estseg', 'etiq_cot', 'etiq_oco', 'etiqprod', 'exclalm', 'exclcte', 'excllug', 'exclprov', 'exi_auto', 'existe', 'EXISTENCIA_20160531_CANTIDADOCOSTONEGATIVO', 'externas', 'facddeta', 'facinico', 'facinive', 'facorser', 'facpdoc', 'facrecc', 'facrecd', 'facreco', 'factentr', 'factudoc', 'facturac', 'facturad', 'factuxm2', 'factuxml', 'factxmld', 'fallas', 'faltasob', 'fcomentr', 'fconcol', 'fconenvioc', 'fconenviod', 'FCONINF', 'FCONORD', 'fcontab', 'festejos', 'filtros', 'flete', 'flujef_r', 'folcajpr', 'folcajve', 'folcarco', 'folcarve', 'folcheco', 'folconal', 'folconci', 'folconcu', 'folcotco', 'folcotes', 'folcotve', 'folcreco', 'folcreve', 'foldevco', 'folestco', 'folfacco', 'FOLFACVE', 'foliosua', 'folmresp', 'folnotav', 'folnotve', 'folordco', 'folordpr', 'folpedve', 'folrefpr', 'folremve', 'folreqco', 'folrespr', 'folstran', 'fondoin', 'frases', 'genera', 'gentxt', 'graficas', 'GRAFPLAN', 'gruposc', 'guiad', 'guias', 'guietiq', 'hisllave', 'hojaprod', 'homepage', 'horextc', 'horextd', 'htmayum', 'icliente', 'iconfvta', 'icotcdeta', 'icotcompc', 'icotcompd', 'icotidet', 'icotizac', 'icotizad', 'imagnta', 'imgform', 'imptos', 'incaxmld', 'indexa', 'ipedidet', 'ipedidoc', 'ipedidod', 'ipedient', 'iprodent', 'istoage', 'istocte', 'istofol', 'istofolp', 'istoprod', 'istoprov', 'kit', 'kit_con', 'kit_cot', 'kit_dec', 'kit_dev', 'kit_fac', 'kit_iped', 'kit_nvta', 'kit_ped', 'kit_rem', 'lecturac', 'lecturad', 'lecturas', 'localiza', 'lote', 'loteod', 'lotesug', 'LUGAALMA', 'lugares', 'mantdeta', 'mantenim', 'mantrefa', 'manttare', 'mapac', 'mapad', 'masivap', 'masivsat', 'mes_crre', 'metodos', 'metp_sat', 'METPAGO', 'modmovil', 'modulos', 'monecfdi', 'MONEDAS', 'monedero', 'moneelec', 'monemovs', 'monsat', 'montdcto', 'montosoc', 'montosrq', 'mordproc', 'mordprod', 'mortizac', 'mortizad', 'motivosc', 'MOVCHE', 'movdetas', 'movdocu', 'movnomc', 'movnomd', 'movrep', 'movs', 'movsalad', 'movsca', 'movsdeta', 'movsentr', 'movserv', 'movsetiq', 'msgsprod', 'ncarclie', 'ncargc', 'ncargv', 'ncarprov', 'ncreclie', 'ncreprov', 'nomacum', 'nomacumd', 'nomaguin', 'nomaguinc', 'nomaguind', 'nomasdic', 'nomasdid', 'nomasdiv', 'nomcaisrc', 'nomcaisrd', 'nomcat', 'nomcat1', 'nomdetper', 'nomfiniq', 'nomform', 'nominc', 'nominc1', 'nomincap', 'nomincc', 'nomincd', 'nomisr', 'nomixm2', 'nomixml', 'nompoli', 'nomptuc', 'nomptud', 'nompues', 'nomsainc', 'nomsaind', 'nomsalmin', 'nomsolva', 'nomsolvad', 'nomsubsidi', 'nomtbls', 'nomtipop', 'nomturno', 'nomvacac', 'nomvacacd', 'notapros', 'notas', 'notclien', 'notidoc', 'notpdoc', 'notpro', 'notvdoc', 'npagos2', 'nventasc', 'nventasd', 'nvtaetiq', 'ocomdeta', 'odatlsp1', 'odatlsp2', 'odatlsp3', 'odatlsp4', 'odatlsp5', 'odatlsp6', 'odatlsp7', 'odatlsp8', 'odatlspc', 'odncetiq', 'ODPEREFA', 'opfadeta', 'opro', 'oprod', 'oprodeta', 'oprodocu', 'oproentr', 'ord_entr', 'orddatos', 'ordproc', 'ordprod', 'ordresau', 'ordrespr', 'oreqdeta', 'otmantde', 'otmante', 'otrefacc', 'otrocomp', 'ottareas', 'pacsd', 'pag2deta', 'pag2docu', 'pag2obs', 'pagcdeta', 'pagcdocu', 'pagcobs', 'pago_age', 'pagoc', 'pagocxml', 'pagos2', 'pagoxm2', 'pagoxmlc', 'pagoxmld', 'pagxdeta', 'pagxdocu', 'pagxobs', 'panelc', 'paneld', 'pantayu', 'peddocu', 'pedexmld', 'PEDIDETA', 'pedidoc', 'PEDIDOD', 'pedidod2', 'PEDIENTR', 'plantic', 'plantid', 'plazos', 'pol_mens', 'polconte', 'POLI_MOV', 'politic2', 'politica', 'polizas', 'polxml', 'porcxdia', 'posfechc', 'POSFECHD', 'prdcamco', 'prdcamcom', 'prds_sat', 'preminv', 'prenomc', 'prenomd', 'prenome', 'prenomh', 'presctas', 'presta', 'prestamo', 'prmfilt', 'prmgrids', 'pro_ctac', 'pro_ctac2', 'pro_ctac3', 'pro_ctap', 'PRO_CTAS', 'pro_desc', 'prod_zap', 'prodana', 'prodequi', 'prodetiq', 'PRODIMAG', 'prodmax', 'PRODMED', 'prodproc', 'producto', 'PRODUNID', 'prog_aut', 'progproc', 'progprod', 'promoc', 'promod', 'promos', 'promox', 'pronvend', 'propieda', 'propprod', 'PROSPECT', 'prosxcla', 'provedor', 'provimag', 'PROVXPRO', 'prvclabe', 'prvsfrxs', 'pxmldoc', 'ranansa', 'ranc_ieps', 'ranc_imp1', 'ranc_imp2', 'ranc_iva', 'ranc_risr', 'ranc_riva', 'ranconta', 'rangopag', 'rangxalm', 'ranp_ieps', 'ranp_iimp', 'ranp_imp1', 'ranp_imp2', 'ranp_iva', 'ranp_risr', 'ranp_riva', 'recnomc', 'recnomd', 'recnome', 'REGIMENS', 'rela_sat', 'remc', 'remd', 'remddeta', 'rementr', 'remidoc', 'reqcdoc', 'requi_c', 'requi_d', 'res_ana', 'resanac', 'resdocu', 'responeq', 'restring', 'resulptu', 'rfc_sub', 'riesgos', 'rprodeta', 'rprodocu', 'ruta_oc', 'ruta_ped', 'ruta_req', 'rutinas', 'rwsrcanp', 'sainalm2', 'sainalma', 'sainctas', 'saldos', 'satws', 'secuenci', 'secxusu', 'segcoti', 'segsec', 'seguicxc', 'separado', 'sepdeta', 'seriedig', 'sernodig', 'setsaimv', 'sgmtoc', 'soliproc', 'soliprod', 'srvcata', 'srvcatp', 'srvchkl', 'srvconf', 'srvcotde', 'srvcotic', 'srvcotid', 'srvcteeq', 'srvcteor', 'srvdatv', 'srvdocu', 'srventr', 'srveqent', 'srvequid', 'srvequif', 'srvequip', 'srvfacc', 'srvfacd', 'srvfacde', 'srvfilt', 'srvgrids', 'srvochk', 'srvofot', 'srvoodt', 'srvordc', 'srvprocs', 'srvproct', 'srvsnot', 'srvsord', 'srvsurde', 'srvsurtd', 'srvxcoag', 'srvxdcag', 'stradeta', 'straentr', 'strandoc', 'stransfc', 'stransfd', 'SUB2CLAS', 'SUBCLASE', 'subtipos', 'subzonas', 'sucursal', 't_consig', 't_ncargo', 't_ncred', 'tab_com', 'TABCOMAG', 'tareas', 'tc_crre', 'tcdiario', 'temas', 'texaviso', 'texcarta', 'textocot', 'texxmld', 'themesd', 'themesh', 'tiplotes', 'tipmovcu', 'tipoalma', 'tipocam', 'tipocont', 'tipoequ', 'tipofal', 'TIPOIVAC', 'tipoivap', 'tipotrab', 'tipprocl', 'tipseg', 'tivanvta', 'TPOIVACA', 'tpoivapa', 'trabajad', 'trans_er', 'transban', 'transext', 'transini', 'transmi', 'txtpreci', 'typetxt', 'ucredcte', 'unidad', 'unidpeso', 'unis_sat', 'uso_sat', 'usoctecs', 'usoctecse', 'USUAALM', 'USUALMAU', 'USUALMSR', 'USUALMST', 'USUARIO2', 'USUARIOS', 'varclpd', 'vcompra', 'vcredito', 'vencidos', 'vital', 'vncargo', 'vpagosc', 'wsrcanp', 'zonas'];

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/')
    ->uses('HomeController@index')
    ->name('index');

Route::get('distribution-channels')
    ->uses('DistributionChannelsController@index')
    ->name('distribution-channels.index');

Route::get('customer-types')
    ->uses('CustomerTypesController@index')
    ->name('customer-types.index');

Route::get('zones')
    ->uses('ZonesController@index')
    ->name('zones.index');

Route::get('subzones')
    ->uses('SubzonesController@index')
    ->name('subzones.index');

Route::apiResource('customers', 'CustomersController');

Route::get('customers/with-credit-available/{id}')
    ->uses('CustomersController@showWithCreditAvailable')
    ->name('customers.with_credit_available');

Route::get('products')
    ->uses('ProductsController@index')
    ->name('products.index');

Route::get('products/inventories')
    ->uses('ProductsController@showAllWithInventory')
    ->name('products.inventories');

Route::get('products/{product}')
    ->uses('ProductsController@show')
    ->name('products.show');
Route::get('products/{product}/{model}')
    ->uses('ProductsController@showWithInventory')
    ->name('products.show-with-inventory');

Route::get('orders/{id}', function ($id) {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();
    $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    $result = getTableData($conn, $sql);

    $sql = 'select * from pedidod where no_ped = '.$id;
    $result2 = getTableData($conn, $sql);

    $code = 200;

    if ($result) {
        $data = $result;
    } else {
        $code = 400;
        $data = [
            'data' => [
                'status' => $code,
                'description' => 'Ocurrio un error en el request',
            ],
        ];
    }

    return $data;
});

Route::get('check', function () use ($saiTables) {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $errors = [];

    foreach ($saiTables as $saiTable) {
        try {
            $sql = dump_sql(DB::table($saiTable)->selectRaw('COUNT(*)'));
            $result = getTableData($conn, $sql);
            $data[$saiTable] = $result;
        } catch (Exception $e) {
            $errors[] = $saiTable;
        }
    }

    return [
        'data' => $data,
        'errors' => $errors,
    ];
});

Route::get('check2', function () {
    $data = [];
    $code;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    // dd(getNextConfigId($conn, 'no_mov', 'CTE'));

    $errors = [];

    $saiTables = [
        // 'config',
        // 'USUARIO2',
        // 'USUARIOS',
        // 'srvgrids',
        // 'CTASCLIE',
        // 'canald',
        'config',
    ];

    foreach ($saiTables as $saiTable) {
        try {
            // $sql = dump_sql(DB::table($saiTable)->limit(20)->orderBy('cve_cte', 'desc'));
            // $sql = dump_sql(DB::connection('sqlsrv')->table($saiTable)->where('clave', 'CTE'));
            $sql = dump_sql(DB::table($saiTable));
            $result = getTableData($conn, $sql);
            $data[$saiTable] = $result;
        } catch (Exception $e) {
            Log::debug($e);

            $errors[] = $saiTable;
        }
    }

    return [
        'data' => $data,
        'errors' => $errors,
    ];
});

Route::get('tabla/{table}/{column}', function ($saiTable, $column) {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $data = getFirst($conn, $saiTable, $column);

    return $data;
});

Route::get('check3', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $lastId = "";
    $rs = $conn->Execute("select top 1 no_mov from movs order by no_mov desc;");
    $lastId = $rs->Fields(0)->value;
    $rs->Close();
    return (string) $lastId;
});

Route::get('test', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = dump_command_sql(DB::table('config')->whereRaw("clave = 'CTE'"), 'Update', [
        'no_mov' => 30816 + 1,
    ]);
    $data = $conn->Execute($sql);

    $data = getTableData($conn, "Select MAX(cve_cte) from clientes;");
    $maxId = (int) Arr::first(Arr::first($data));
    $customerId = (int) getNextConfigId($conn, 'no_mov', 'CTE');

    dd(compact('data', 'customerId'));
});

Route::get('pedidos', function () {

    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = dump_sql(DB::table('pedidoc')->whereRaw('(f_alta_ped <= CTOD(\''.date("m-d-Y").'\') AND CTOD(\''.date("m")."-01-".date("Y").'\') <= f_alta_ped)'));

    // Log::debug("SQL pedidos", [$sql]);
    $data = getTableData($conn, $sql);

    if (is_null($record = Arr::first($data))) {
        return response('', 404);
    }

    $orders = collect([]);

    foreach ($data as $data_order) {
        $order = new Fluent($data_order);
        // Log::debug("DATA_ORDER", [$order]);

        $sql = dump_sql(DB::table('pedidod')->where('no_ped', (int) $order->no_ped));
        $data = getTableData($conn, $sql);
        $order->lines = collect($data);

        $orders->push($order);
    }

    return $orders;
});

Route::get('pedidos/{order}/invoices/{format}')
    ->uses('OrdersController@invoices')
    ->name('orders.invoices.file');

Route::get('pedidos/porsurtir', function () {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = "Select no_ped, status from pedidoc where lugar = 'MTY' and status = 'Por Surtir' and no_ped >= 179720";
    $data = getTableData($conn, $sql);

    return $data;
});

Route::get('pedidos/{id}', function ($id) {
    $con = new VFPConnector();
    $conn = $con->getConnection();

    $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);

    if (is_null($record = Arr::first($data))) {
        return response('', 404);
    }

    $order = new Fluent($record);

    $sql = dump_sql(DB::table('pedidod')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);
    $order->lines = collect($data);

    return $order;
});

Route::post('pedidos', function () {
    $code = 200;
    $allPostParams = request()->all();
    // Log::debug("PEDIDOS POST REQUEST", [$allPostParams]);
    $valParam = validateParams($allPostParams); //primero valido los parámetros

    $data = [];

    if ($valParam == "OK") {
        $VFPConnObj = new VFPConnector();

        // $MYSQLConnObj = new MySQLConnection();
        $VFPConn = $VFPConnObj->getConnection();

        // $mySQLConn = $MYSQLConnObj->getConnection();
        $observa = $allPostParams['data']['observa'];
        $lista_pre = $allPostParams['data']['lista_pre'];
        $cve_cte = $allPostParams['data']['cve_cte'];
        $cve_age = $allPostParams['data']['cve_age'];
        $usuario = $allPostParams['data']['usuario'];
        // $lista_precData = getListaPorCliente($VFPConn, $cve_cte);
        // $lista_prec = $lista_precData[0]['lista_prec'];

        $subtotalTotal = 0;
        $totalTotal = 0;
        $ivaTotal = 0;
        $firstTime = true;

        //checamos si tiene descuento
        $descue = 0;
        $descuento = 0;
        $descue = isset($allPostParams['data']['descuento']) ? $allPostParams['data']['descuento'] : null;
        if (!($descue && is_numeric($descue))) {
            $descue = 0;
            $descuento = 0;
        } else {
            $descue = floatval($descue);
        }

        foreach ($allPostParams['data']['pedidos'] as $pedido) {
            //sacamos el precio TOTAL de todos los pedidos para insertar en pedidoc antes
            $precio = $pedido['precio'];
            // }
            // pre($precioData);
            // die();
            $cantidad = $pedido['cantidad'];
            $subtotalTotal += $precio * $cantidad;
        }

        $lastId = getNextPedidocId($VFPConn);
        $lastId = intval($lastId);
        $lastId++;
        setNextConfigId($VFPConn, 'CONSPEDMAT', ($lastId + 1));

        $lastMovsId = getNextMovsId($VFPConn);
        $lastMovsId = intval($lastMovsId);
        $lastMovsId++;

        foreach ($allPostParams['data']['envios'] as $envio) {
            $cantidad = intval($envio['cantidad']);
            $precio = floatval($envio['precio']);

            $dataD = array();
            $dataD['no_ped'] = intval($lastId);
            $dataD['cve_prod'] = $envio['cve_prod']; // "'".$enviosArr[0]['cve_prod']."'";
            $dataD['cse_prod'] = !empty($envio['cse_prod']) ? $envio['cse_prod'] : ""; // "'".$enviosArr[0]['cse_prod']."'";
            $dataD['cant_prod'] = floatval($cantidad);
            $dataD['valor_prod'] = floatval($envio['precio']);
            $dataD['cve_suc'] = "MAT";
            $dataD['new_med'] = "";
            $dataD['lista_pre'] = $lista_pre;
            $dataD['saldo'] = intval($cantidad);
            $dataD['fol_prod'] = 1;
            insertPedidod($VFPConn, $dataD);

            $subtotalTotal += $precio * $cantidad;
        }
        $descuento = floatval($subtotalTotal * $descue / 100);
        // $subtotalTotal = $subtotalTotal - $descuento;
        $ivaTotal = floatval(($subtotalTotal - $descuento) * .16);
        $totalTotal = floatval($subtotalTotal) - $descuento + floatval($ivaTotal);

        foreach ($allPostParams['data']['pedidos'] as $index => $pedido) {
            $index++;
            $precio = $pedido['precio'];
            $descuentopv = isset($pedido['descuento']) ? $pedido['descuento'] : 0;
            $cse_prod = $pedido['cse_prod'];
            $cantidad = $pedido['cantidad'];
            $cve_prod = $pedido['cve_prod'];

            $color = $pedido['new_med'];
            $cto_ent = floatval($pedido['cto_ent']);
            if ($firstTime) {
                //solo se inserta un pedidoc por todos los pedidos
                $data = array();
                $data['no_ped'] = intval($lastId);
                $data['subt_ped'] = floatval($subtotalTotal);
                $data['iva'] = $ivaTotal;
                $data['total_ped'] = $totalTotal;

                $data['observa'] = "'".$observa."'";
                $data['cve_suc'] = "'MAT'";
                $data['usuario'] = $cve_age;
                $data['trans'] = 1;
                $data['lugar'] = "'MTY'";
                $data['cve_cte'] = $cve_cte;
                $data['cve_age'] = $cve_age;
                $data['descuento'] = $descuento;
                $data['descue'] = $descue;
                $data['usuario'] = intval($usuario);

                $datapedientr = array();
                $datapedientr['cve_suc'] = "MAT";
                $datapedientr['no_ped'] = intval($lastId);
                $datapedientr['nom_ent'] = $observa;

                insertPedidoc($VFPConn, $data);
                insertPedientr($VFPConn, $datapedientr);
                $dataFolPedVe = array();
                $dataFolPedVe['no_ped'] = intval($lastId);
                $dataFolPedVe['cve_age'] = $cve_age;
                $dataFolPedVe['cve_cte'] = $cve_cte;
                insertFolPedVe($VFPConn, $dataFolPedVe);

                $firstTime = false;
            }

            $dataD = array();
            $dataD['no_ped'] = intval($lastId);
            $dataD['cve_prod'] = $cve_prod;
            $dataD['cse_prod'] = !empty($cse_prod) ? $cse_prod : "";
            $dataD['cant_prod'] = floatval($cantidad);
            $dataD['valor_prod'] = floatval($precio);
            $dataD['cve_suc'] = "MAT";
            $dataD['new_med'] = !empty($color) ? $color : "";
            $dataD['saldo'] = intval($cantidad);
            $dataD['lista_pre'] = $lista_pre;
            $dataD['fol_prod'] = ($index + 1);

            insertPedidod($VFPConn, $dataD);
        }
        $data = [
            'data' => [
                'status' => $code,
                'descripcion' => 'Pedido insertado con exito',
                'noPedido' => $lastId,
            ],
        ];
    } else {
        $code = 500;
        $data = [
            'data' => [
                'error' => [
                    'status' => $code,
                    'descripcion' => $valParam,
                ],
            ],
        ];
    }

    return response()->json($data, $code);
});

Route::post('pedidos/{id}/remision', function ($id) {
    $code = 200;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    // Obtenemos el pedido
    $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);

    if (is_null($record = Arr::first($data))) {
        return response('', 404);
    }

    $order = new Fluent($record);

    // Le añadimos los productos como partidas
    $sql = dump_sql(DB::table('pedidod')->where('no_ped', (int) $id));
    $data = getTableData($conn, $sql);
    $order->lines = collect($data);

    // Obtenemos el consecutivo de la remisión
    $lastId = getNextRemcId($conn);
    $lastId = intval($lastId);
    $lastId++;
    setNextConfigId($conn, 'CONSREMMAT', ($lastId + 1));

    $datac = array();
    $datac['no_rem'] = intval($lastId);
    $datac['cve_suc'] = $order->cve_suc;
    $datac['cve_cte'] = intval($order->cve_cte);
    $datac['cve_age'] = intval($order->cve_age);
    $datac['observa'] = $order->observa;
    $datac['lugar'] = $order->lugar;
    $datac['usuario'] = intval($order->usuario);
    $datac['trans'] = intval($order->trans);
    $datac['descue'] = floatval($order->descue);
    $datac['subt_ped'] = floatval($order->subt_ped);
    $datac['descuento'] = floatval($order->descuento);
    $datac['total_ped'] = floatval($order->total_ped);

    $datarementr = array();
    $datarementr['suc_rem'] = $order->cve_suc;
    $datarementr['no_rem'] = intval($lastId);
    $datarementr['nom_ent'] = $order->observa;

    insertRemc($conn, $datac); // Creamos el registro de la remisión
    insertRementr($conn, $datarementr); // Creamos la entrada de la remisión
    $datafolremve = array();
    $datafolremve['no_rem'] = intval($lastId);
    $datafolremve['cve_age'] = intval($order->cve_age);
    $datafolremve['cve_cte'] = intval($order->cve_cte);
    insertFolRemVe($conn, $datafolremve);

    $lastMovsId = (int) getNextConfigId($conn, 'no_mov', 'MOV');

    foreach ($order->lines as $index => $line) {
        $numpol = (int) getNextNumPol($conn);

        $index++;

        $dataD = array();
        $dataD['part_rem'] = intval($index);
        $dataD['part_ped'] = intval($index);
        // $dataD['part_ped'] = intval($line['fol_prod']);

        $dataD['no_rem'] = intval($lastId);
        $dataD['no_ped'] = intval($line['no_ped']);
        $dataD['cve_prod'] = $line['cve_prod'];
        $dataD['cse_prod'] = $line['cse_prod'];
        $dataD['cant_prod'] = floatval($line['cant_prod']);
        $dataD['valor_prod'] = floatval($line['valor_prod']);
        $dataD['cve_suc'] = $line['cve_suc'];
        $dataD['new_med'] = $line['new_med'];
        $dataD['saldo'] = floatval($line['cant_prod']);
        $dataD['descu_prod'] = 0.00;
        $dataD['lista_pre'] = $line['lista_pre'];

        insertRemd($conn, $dataD); // Creamos la partida de la remisión

        $dataMov = array();
        $dataMov['no_mov'] = intval($lastMovsId);
        $dataMov['cve_prod'] = $line['cve_prod'];
        $dataMov['no_ref'] = intval($lastId);
        $dataMov['cant_prod'] = floatval($line['cant_prod']);
        $dataMov['costo_ent'] = floatval($line['valor_prod']);
        $dataMov['cto_prod'] = floatval($line['valor_prod']);
        $dataMov['preci_prod'] = floatval($line['valor_prod']);
        $dataMov['cve_age'] = intval($order->cve_age);
        $dataMov['cve_cte'] = intval($order->cve_cte);
        $dataMov['new_med'] = $line['new_med'];
        $dataMov['tipo_mov'] = "S";
        $dataMov['cve_mov'] = "20";
        $dataMov['partida'] = intval($index);
        $dataMov['no_rem'] = intval($lastId);
        $dataMov['numpol'] = $numpol;
        $dataMov['usuario'] = intval($order->usuario);

        insertMovs($conn, $dataMov); // Creamos el movimiento de SALIDA(S) en el sistema

        $sql = dump_command_sql(DB::table('config')->whereRaw("clave = 'MOV'"), 'Update', [
            'no_mov' => intval($lastMovsId + 1),
        ]);
        $data = $conn->Execute($sql); // Actualizamos el número de movimientos actuales

        /// AGREGAMOS POLIZA //////////////////////////////////////////
        $poliza = array();

        $poliza['numpol'] = $numpol;
        $poliza['conc_pol'] = "Salida de almacén # ".intval($lastMovsId)." por remisión ref.# ".intval($lastId);
        $poliza['cargos'] = floatval($line['valor_prod']);
        $poliza['abonos'] = floatval($line['valor_prod']);
        $poliza['tipo_pol'] = "23";
        $poliza['no_mov'] = intval($lastMovsId);
        $poliza['suc_mov'] = "MAT";
        $poliza['usuario'] = intval($order->usuario);

        insertPoliza($conn, $poliza);
        ///////////////////////////////////////////////////////////////

        $sqlPedidod = dump_command_sql(DB::table('pedidod')->whereRaw("no_ped = {$line['no_ped']}")->whereRaw("cve_prod = '{$line['cve_prod']}'"), 'Update', [
            'saldo' => 0,
            'status1' => 'S',
        ]);
        $dataPedidod = $conn->Execute($sqlPedidod); //Update pedidod - restamos saldo de pedido de cada producto, para que se descuente de inventario y le damos estatus(status1) de salida(S)

        decrementInventory($conn, $order->lugar, $line['cve_prod'], $line['new_med'], $line['cant_prod']); // Restamos el inventario
    }

    $sqlPedidoc = dump_command_sql(DB::table('pedidoc')->whereRaw("no_ped = {$order->no_ped}"), 'Update', [
        'status' => 'Surtido',
    ]);
    $dataPedidoc = $conn->Execute($sqlPedidoc); //Update pedidoc - actualizamos el estatus de Por Surtir a Surtido

    $data = [
        'data' => [
            'status' => $code,
            'descripcion' => 'Remisión creada con exito',
            'noRemision' => $lastId,
        ],
    ];

    return response()->json($data, $code);
});

Route::get('pedidos/{id}/remision', function ($id) {
    $data = [];
    $code = 200;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    // // Validamos que el pedido exista
    // $sql = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
    // $data = getTableData($conn, $sql);

    // // Si pedido existe lo ponemos en objeto
    // if (is_null($record = Arr::first($data))) {
    //     return response('', 404);
    // }

    // $order = new Fluent($record);

    // Revisamos si el pedido tiene remisión relacionada
    $remision = null;

    // Validamos si existe la remisión del pedido
    $sql_remd = dump_sql(DB::table('remd')->where('no_ped', (int) $id)->selectRaw("top 1 `no_ped`, `no_rem`")->orderByDesc("part_rem"));
    $data_remd = getTableData($conn, $sql_remd);

    // Si existe instanciamos la remisión
    if (!is_null($remd_row = Arr::first($data_remd))) {
        $remd = new Fluent($remd_row);
        $remision_id = $remd->no_rem;

        $sql_remc = dump_sql(DB::table('remc')->where('no_rem', (int) $remision_id));
        $data_remc = getTableData($conn, $sql_remc);

        if (is_null($remc_record = Arr::first($data_remc))) {
            $remision = null; // No existe la remisión
        } else {
            $remision = new Fluent($remc_record);

            // revisamos si ya esta facturado
            if ($remision->status_rem = "Facturada") {
                // Si la remision esta facturada obtenemos el folio de factura
                $sql = dump_sql(DB::table('facturad')->where('no_rem', (int) $remision->no_rem));
                $data = getTableData($conn, "select top 1 no_fac from facturad where pedi_rem = {$remision->no_rem} order by no_fac desc;");

                if (is_null($record_facturad = Arr::first($data))) {
                    Log::debug("No hay respuesta de factura en la tabla facturad");
                } else {
                    $invoice = new Fluent($record_facturad);
                    $remision->no_fac = $invoice->no_fac;
                }
            }
        }
    } else {
        return response('', 404);
    }

    if (!is_null($remision)) {
        return $remision;
    } else {
        return response('', 404);
    }
});

Route::get('remisiones/{id}', function ($id) {
    $data = [];
    $code = 200;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    // Revisamos si el pedido tiene remisión relacionada
    $remision = null;

    // Si existe instanciamos la remisión
    $sql_remc = dump_sql(DB::table('remc')->where('no_rem', (int) $id));
    $data_remc = getTableData($conn, $sql_remc);

    if (is_null($remc_record = Arr::first($data_remc))) {
        $remision = null; // No existe la remisión
    } else {
        $remision = new Fluent($remc_record);

        // revisamos si ya esta facturado
        if ($remision->status_rem = "Facturada") {
            // Si la remision esta facturada obtenemos el folio de factura
            $sql = dump_sql(DB::table('facturad')->where('no_rem', (int) $remision->no_rem));
            $data = getTableData($conn, "select top 1 no_fac from facturad where no_rem = {$remision->no_rem} order by no_fac desc;");

            if (is_null($record_facturad = Arr::first($data))) {
                Log::debug("No hay respuesta de factura en la tabla facturad");
            } else {
                $invoice = new Fluent($record_facturad);
                $remision->no_fac = $invoice->no_fac;
            }
        }
    }

    if (!is_null($remision)) {
        return $remision;
    } else {
        return response('', 404);
    }
});

Route::post('pedidos/{id}/cancelar', function ($id) {
    $data = [];
    $code = 200;
    $con = new VFPConnector();
    $conn = $con->getConnection();

    try {

        // Revisamos si el pedido tiene remisión relacionada

        $remision = null;

        // Obtenemos la remisión del pedido
        $sql_remd = dump_sql(DB::table('remd')->where('no_ped', (int) $id)->selectRaw("top 1 `no_ped`, `no_rem`")->orderByDesc("part_rem"));
        $data_remd = getTableData($conn, $sql_remd);

        if (!is_null($remd_row = Arr::first($data_remd))) {
            $remd = new Fluent($remd_row);
            $remision_id = $remd->no_rem;
            // Log::debug("REMD EXISTE", [$remd]);

            $sql_remc = dump_sql(DB::table('remc')->where('no_rem', (int) $remision_id));
            $data_remc = getTableData($conn, $sql_remc);

            if (is_null($remc_record = Arr::first($data_remc))) {
                $remision = null; // No existe la remisión
                // Log::debug("REMISION NO EXISTE");
            } else {
                $remision = new Fluent($remc_record);
                // Le añadimos los productos como partidas
                $sql_remd = dump_sql(DB::table('remd')->where('no_rem', (int) $remision_id));
                $data_remd = getTableData($conn, $sql_remd);
                $remision->lines = collect($data_remd);

                // Log::debug("REMISION EXISTE", [$remision]);

                $sql_remc = dump_command_sql(DB::table('remc')->whereRaw("no_rem = {$remision->no_rem}"), 'Update', [
                    'status_rem' => 'Cancelada',
                ]);
                $data_remc = $conn->Execute($sql_remc); //Update remc - actualizamos el estatus(status_rem) a Cancelada

                $lastMovsId = (int) getNextConfigId($conn, 'no_mov', 'MOV');

                // Log::debug("REMISION LINES", [$remision->lines->count()]);

                if ($remision->lines->count() > 0) {

                    // Log::debug("REMISION LINES ES > 0");

                    foreach ($remision->lines as $index => $line) {
                        // Log::debug("REMISION FOREACH INDEX", [$index]);
                        // Log::debug("REMISION FOREACH LINE", [$line]);
                        $numpol = (int) getNextNumPol($conn);
                        // Log::debug("getNextNumPol", [$numpol]);

                        $dataMov = array();
                        $dataMov['no_mov'] = intval($lastMovsId);
                        $dataMov['cve_prod'] = $line['cve_prod'];
                        $dataMov['no_ref'] = $line['no_rem'];
                        $dataMov['cant_prod'] = floatval($line['cant_prod']);
                        $dataMov['costo_ent'] = floatval($line['valor_prod']);
                        $dataMov['cto_prod'] = floatval($line['valor_prod']);
                        $dataMov['preci_prod'] = floatval($line['valor_prod']);
                        $dataMov['cve_age'] = intval($remision->cve_age);
                        $dataMov['cve_cte'] = intval($remision->cve_cte);
                        $dataMov['new_med'] = $line['new_med'];
                        $dataMov['tipo_mov'] = "E";
                        $dataMov['cve_mov'] = "19";
                        $dataMov['orig_mov'] = "";
                        $dataMov['dest_mov'] = "MTY";
                        $dataMov['partida'] = intval($index + 1);
                        $dataMov['no_rem'] = intval($line['no_rem']);
                        $dataMov['usuario'] = intval($remision->usuario);

                        // Log::debug("insertMovs data", [$dataMov]);
                        $insertMovsRes = insertMovs($conn, $dataMov); // Creamos el movimiento de ENTRADA(E) en el sistema

                        // Log::debug("insertMovs result", [$insertMovsRes]);

                        $sql = dump_command_sql(DB::table('config')->whereRaw("clave = 'MOV'"), 'Update', [
                            'no_mov' => intval($lastMovsId + 1),
                        ]);
                        $data = $conn->Execute($sql); // Actualizamos el número de movimientos actuales

                        // Log::debug("actualizar config result", [$data]);

                        /// AGREGAMOS POLIZA //////////////////////////////////////////
                        $poliza = array();

                        $poliza['numpol'] = $numpol;
                        $poliza['conc_pol'] = "Entrada de almacén # ".intval($lastMovsId)." por can. de remisión ref.# ".intval($remision->no_rem);
                        $poliza['cargos'] = floatval($line['valor_prod']);
                        $poliza['abonos'] = floatval($line['valor_prod']);
                        $poliza['tipo_pol'] = "21";
                        $poliza['no_mov'] = intval($lastMovsId);
                        $poliza['suc_mov'] = "MAT";
                        $poliza['usuario'] = intval($remision->usuario);

                        $polizares = insertPoliza($conn, $poliza);

                        // Log::debug("insertPoliza data", [$poliza]);
                        // Log::debug("insertPoliza result", [$polizares]);
                        ///////////////////////////////////////////////////////////////

                        $sqlPedidod = dump_command_sql(DB::table('pedidod')->whereRaw("no_ped = {$line['no_ped']}")->whereRaw("cve_prod = '{$line['cve_prod']}'"), 'Update', [
                            'saldo' => floatval($line['cant_prod']),
                            'status1' => 'C',
                        ]);
                        $dataPedidod = $conn->Execute($sqlPedidod); //Update pedidod - restamos saldo de pedido de cada producto, para que se descuente de inventario y le damos estatus(status1) de salida(S)
                        // Log::debug("actualizar pedidod sql", [$sqlPedidod]);
                        // Log::debug("actualizar pedidod result", [$dataPedidod]);

                        $incrementInventoryres = incrementInventory($conn, $remision->lugar, $line['cve_prod'], $line['new_med'], $line['cant_prod']); // Restamos el inventario
                        // Log::debug("incrementInventory result", [$incrementInventoryres]);
                    }
                }
            }
        } else {
            // Log::debug("REMD NO EXISTE");
            // Obtenemos el pedido
            $sql_pedidoc = dump_sql(DB::table('pedidoc')->where('no_ped', (int) $id));
            $data_pedidoc = getTableData($conn, $sql_pedidoc);

            if (is_null($record_pedido = Arr::first($data_pedidoc))) {
                $pedido = null; // No existe la remisión
                // Log::debug("PEDIDO NO EXISTE");
            } else {
                $pedido = new Fluent($record_pedido);
                // Log::debug("PEDIDO EXISTE", [$pedido]);
            }

            if (!is_null($pedido)) {
                // Le añadimos los productos como partidas
                $sql = dump_sql(DB::table('pedidod')->where('no_ped', (int) $id));
                $data = getTableData($conn, $sql);
                $pedido->lines = collect($data);

                $lastMovsId = (int) getNextConfigId($conn, 'no_mov', 'MOV');

                if ($pedido->lines->count() > 0) {
                    foreach ($pedido->lines as $index => $line) {
                        $sqlPedidod = dump_command_sql(DB::table('pedidod')->whereRaw("no_ped = {$line['no_ped']}")->whereRaw("cve_prod = '{$line['cve_prod']}'"), 'Update', [
                            'saldo' => floatval($line['cant_prod']),
                            'status1' => 'C',
                        ]);
                        $dataPedidod = $conn->Execute($sqlPedidod); //Update pedidod - restamos saldo de pedido de cada producto, para que se descuente de inventario y le damos estatus(status1) de salida(S)
                    }
                }
            }
        }

        // Regresamos el status del pedido a "Por Surtir"
        $sqlPedidoc = dump_command_sql(DB::table('pedidoc')->whereRaw("no_ped = {$id}"), 'Update', [
            'status' => 'Cancelado',
        ]);
        $dataPedidoc = $conn->Execute($sqlPedidoc); //Update pedidoc - actualizamos el estatus de Por Surtir a Surtido

        // $sql = "update pedidoc set status = 'Cancelado' where no_ped = " . $id;
        // $rs = $conn->Execute($sql);
        // $sql = "update pedidod set status1 = 'C' where no_ped = " . $id;
        // $rs = $conn->Execute($sql);

        return "OK";
    } catch (Exception $e) {
        return "Error:".$e->getMessage();
    }
});
